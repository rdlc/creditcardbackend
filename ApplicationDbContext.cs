using Microsoft.EntityFrameworkCore;
using CreditCardBackend.Models;

namespace CreditCardBackend;

public class ApplicationDbContext : DbContext
{
    public DbSet<CreditCard> CreditCard { get; set; }

    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<CreditCard>(creditcard =>
       {
           creditcard.ToTable("CreditCard");
           creditcard.HasKey(p => p.creditCardId);
           creditcard.Property(p => p.cardHolder).IsRequired().HasMaxLength(100);
           creditcard.Property(p => p.cardNumber).IsRequired().HasMaxLength(16);
           creditcard.Property(p => p.expiredDate).IsRequired().HasMaxLength(5);
           creditcard.Property(p => p.cvv).IsRequired().HasMaxLength(3);
       });
    }

}