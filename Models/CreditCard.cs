
namespace CreditCardBackend.Models;

public class CreditCard
{
    public int creditCardId { get; set; }
    public string cardHolder { get; set; }
    public string cardNumber { get; set; }
    public string expiredDate { get; set; }
    public string cvv { get; set; }
}