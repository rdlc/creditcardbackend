﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CreditCardBackend.Migrations
{
    public partial class FirstMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CreditCard",
                columns: table => new
                {
                    creditCardId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    cardHolder = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    cardNumber = table.Column<string>(type: "nvarchar(16)", maxLength: 16, nullable: false),
                    expiredDate = table.Column<string>(type: "nvarchar(5)", maxLength: 5, nullable: false),
                    cvv = table.Column<string>(type: "nvarchar(3)", maxLength: 3, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CreditCard", x => x.creditCardId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CreditCard");
        }
    }
}
