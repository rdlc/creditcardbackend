
using Microsoft.AspNetCore.Mvc;

using CreditCardBackend.Models;
using CreditCardBackend.Services;

namespace CreditCardBackend.Controllers;

[Route("api/[controller]")]
public class CreditCardController : ControllerBase
{
    ICreditCardService creditCardService;

    public CreditCardController(ICreditCardService service)
    {
        creditCardService = service;
    }

    [HttpGet]
    public IActionResult Get()
    {
        return Ok(creditCardService.Get());
    }

    [HttpPost]
    public IActionResult Post([FromBody] CreditCard _creditCard)
    {
        creditCardService.Save(_creditCard);
        return Ok();
    }

    [HttpPut("{id}")]
    public IActionResult Put(int id, [FromBody] CreditCard _creditCard)
    {
        creditCardService.Update(id, _creditCard);
        return Ok();
    }

    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
        creditCardService.Delete(id);
        return Ok();
    }
}