
using CreditCardBackend.Models;

namespace CreditCardBackend.Services;


public class CreditCardService : ICreditCardService
{
    ApplicationDbContext context;

    public CreditCardService(ApplicationDbContext dbContext)
    {
        context = dbContext;
    }

    public IEnumerable<CreditCard> Get()
    {
        return context.CreditCard;
    }

    public async Task Save(CreditCard creditCard)
    {
        context.Add(creditCard);
        await context.SaveChangesAsync();
    }

    public async Task Update(int id, CreditCard creditCard)
    {
        var creditCardToUpdate = context.CreditCard.Find(id);

        if (creditCardToUpdate != null)
        {
            creditCardToUpdate.cardHolder = creditCard.cardHolder;
            creditCardToUpdate.cardNumber = creditCard.cardNumber;
            creditCardToUpdate.expiredDate = creditCard.expiredDate;
            creditCardToUpdate.cvv = creditCard.cvv;

            await context.SaveChangesAsync();
        }
    }

    public async Task Delete(int id)
    {
        var creditCardToDelete = context.CreditCard.Find(id);

        if (creditCardToDelete != null)
        {
            context.Remove(creditCardToDelete);
            await context.SaveChangesAsync();
        }
    }
}

public interface ICreditCardService
{
    IEnumerable<CreditCard> Get();
    Task Save(CreditCard creditCard);
    Task Update(int id, CreditCard creditCard);
    Task Delete(int id);
}